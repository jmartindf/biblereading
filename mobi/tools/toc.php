<!-- Table of Contents -->
<!-- TABLE OF CONTENTS -->
<mbp:pagebreak />
<guide> <reference type="toc" title="Table of Contents" href="#toc"/> </guide>
<a name="toc"/><h1>Table of Contents</h1>
<?php
for ($day = $start; $day <= $end; $day++) {
  if($zBased) {
    $dayOut = $day+1;
  } else {
    $dayOut = $day;
  }
?>
  <p><a href="#day-<?php echo $dayOut; ?>">Day <?php echo $dayOut; ?></a></p>
<?php
}
?>
<!-- End ToC -->
