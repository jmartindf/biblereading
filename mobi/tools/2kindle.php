<?php
if(sizeof($argv)!=4) {
  fwrite(STDERR, "Wrong number of parameters.\n");
  fwrite(STDERR, "2kindle.php startDay endDay planFile outputFile\n");
	exit(1); // A response code other than 0 is a failure
}

$proj_root = realpath(joinPaths(dirname(__FILE__),"..",".."));
$cache_dir = realpath(joinPaths($proj_root,"mobi","cache"));
$out_dir = realpath(joinPaths($proj_root,"mobi","output"));
$plan_dir = realpath(joinPaths($proj_root,"plans","output"));

$start = $argv[2];
$end = $argv[3];
$file = joinPaths($plan_dir,$argv[1].".json");

$json = file_get_contents($file);
$data = json_decode($json, True);
$planName = $data["name"];
$safeName = sanitize_plan_name($planName);

$oBase = $safeName.$start."-".$end;
$oHTML = $oBase.".html";
$oMOBI = $oBase.".mobi";

$output = joinPaths($out_dir,$oHTML);
$mobi_output = joinPaths($out_dir,$oMOBI);

$readings = array();
$zBased = false;
foreach ($data["readings"] as $r) {
  $plan = intval($r['plan']);
  if ($plan==0) {
    $zBased = true;
  }
	$reference = $r['reference'];
	$refcnt = $r['refcnt'];
	$readings[$plan][$refcnt] = $reference;
}
$title = $planName.", Days $start-$end";
if ($zBased) {
  $start--;
  $end--;
}
ob_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <title><?php echo $title; ?></title>
  <meta name="author" content="Joe Martin">
  <meta name="viewport" content="width = 320" />
  <!-- Date: <?php echo date("Y-m-d"); ?> -->
</head>
<body>
<a name="start"/> <!-- Kindle tag for the beginning of the document -->
<?php
for ($day = $start; $day <= $end; $day++) {
  if($zBased) {
    $dayOut = $day+1;
  } else {
    $dayOut = $day;
  }
	$passage = "";
	foreach ($readings[$day] as $ref) {
		$passage = $passage . ";" . $ref;
	}
  $passage = substr($passage,1);
  if ($passage == "") {
    continue;
  }
?>
<a name="day-<?php echo $dayOut; ?>"><h1 class="chapter">Day <?php echo $dayOut; ?></h1></a>
<p style="font-size:large"> <?php echo dayofyear2date($dayOut-1, "F j"); ?> </p>
<hr />
<p>
<?php
  foreach($readings[$day] as $ref) {
    echo $ref."<br />\n";
  }
?>
</p>
<div class="readings">
<?php echo get_passage_html($passage,$dayOut,$safeName,$cache_dir); ?>
</div>
<mbp:pagebreak />
<?php } ?>

</body>
</html>

<?php
$data = ob_get_contents(); 
$fp = fopen ($output, "w"); 
fwrite($fp, $data);
fclose($fp);
ob_end_clean();

make_mobi($title,$output,$mobi_output);

function make_mobi($title,$html,$mobi) {
  $author = "ESV";
  $output = array();
  echo "generating book '$title' by '$author'\n";
  $cmd_args = array(
    "ebook-convert",
    "\"$html\"",
    "\"$mobi\"",
    "--output-profile kindle",
    "--insert-blank-line",
    "--authors \"$author\"",
    "--title \"$title\""
 );
  $command = implode(" ",$cmd_args);
  exec($command,$output,$return);
}

function sanitize_plan_name($name) {
  $escArray = array(
    "[",
    "]",
    "(",
    ")",
    " ",
    ",",
    "'",
    "\"",
    "|",
    "&",
    "<",
    ">",
    "$",
    "`",
    "\\",
    "\t",
    "*",
    "?",
    "#",
    "~",
    "=",
    "%"
  );
  $name = str_ireplace($escArray,"",$name);
  return $name;
}

function get_passage_html($passage,$day,$plan,$cache) {
  $fname = joinPaths($cache,$plan,$day.".html");
  $path=dirname($fname);
  if(!is_dir($path)) {
    mkdir($path);
  }
  if(file_exists($fname)) {
    $contents = file_get_contents($fname);
    return $contents;
  } else {
    $contents = get_passage_html_web($passage,$day);
    if(is_dir($path)) {
      file_put_contents($fname,$contents);
    } else {
      fwrite(STDERR, "Couldn't cache output to $path\n");
    }
    return $contents;
  }
}

function joinPaths() {
  $paths = array_filter(func_get_args());
  return preg_replace('#/{2,}#', '/', implode('/', $paths));
}

function get_passage_html_web($passage,$day) {
	$options = array(
		'key=IP',
		'include-first-verse-numbers=0',
		'include-verse-numbers=0',
		'include-headings=0',
		'include-audio-link=0',
		'passage='.urlencode($passage)
	);
	$optionStr = join('&',$options);
	$url = "http://www.esvapi.org/v2/rest/passageQuery?";
	$query = $url.$optionStr;
	$response = "";

	$data = fopen($query, "r") ;

	if ($data) {
		while (!feof($data)) {
			$buffer = fgets($data, 4096);
			$response = $response.$buffer;
		}
		fclose($data);
	} else {
		die("fopen failed for url to webservice");
	}
	$response = str_replace("<h2>","<h2 class=\"chapter\">",$response);
	$response = str_replace("#f","#day".$day."f",$response);
	$response = str_replace("id=\"b","id=\"day".$day."b",$response);
	$response = str_replace("#b","#day".$day."b",$response);
	$response = str_replace("id=\"f","id=\"day".$day."f",$response);
	return $response;
}

function dayofyear2date( $tDay, $tFormat = 'd-m-Y' ) {
    $day = intval( $tDay );
    $day = ( $day == 0 ) ? $day : $day - 1;
    $offset = intval( intval( $tDay ) * 86400 );
    $str = date( $tFormat, strtotime( 'Jan 1, ' . date( 'Y' ) ) + $offset );
    return( $str );
} // http://www.iamboredsoiblog.eu/2008/11/01/calculate-date-from-day-of-year-in-php/
?>
