<h1>Slackers Reading for <?php echo $dayName; ?></h1>
<?php echo $reading; ?>
<hr />
<div><p><a id="updateLink" href="update.php?id=<?php echo $id; ?>">Done with this passage.</a></p></div>
<script type="text/javascript">
$("#updateLink").click (function () {
  $.ajax({
    type: "POST",
    url: "update.php",
    data: {
      id: <?php echo $id; ?>
    },
    success: function(data){
      $("#content").html(data);
      window.scrollTo(0,0);
    }
  });
  return false; // stop the browser following the link
});
</script>
