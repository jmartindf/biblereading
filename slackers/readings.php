<?php
include_once "funcs.php";
$data = getPassage($dbpath,$day);
if( isset($data['passage']) ) {
  $reading = getReading($data['passage']);
  $id = $data['id'];
  include "body.php";
} else { ?>
<h1>Slackers Reading for <?php echo $dayName; ?></h1>
<p>Today's reading is missing.</p>
<?php
}
?>
