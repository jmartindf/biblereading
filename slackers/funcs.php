<?php
include "settings.php";

function getReading($passage="") {
	$options = array(
		'key=IP',
		'include-first-verse-numbers=0',
		'include-verse-numbers=0',
		'include-headings=1',
		'include-audio-link=1',
		'passage='.urlencode($passage)
	);
	$optionStr = join('&',$options);
	$url = "http://www.esvapi.org/v2/rest/passageQuery?";
	$query = $url.$optionStr;
	$response = "";
	
	$data = fopen($query, "r") ;

	if ($data) {
		while (!feof($data)) {
			$buffer = fgets($data, 4096);
			$response = $response.$buffer;
		}
		fclose($data);
	} else {
		die("fopen failed for url to webservice");
	}
	return $response;
}

function getPassage($dbpath, $day) {
  $data = array('passage' => null, 'id' => null);
  try {
    $dbh = new PDO("sqlite:$dbpath/readings.db");
    $sth = $dbh->prepare("select id,passage from readings where day=:day and read=0 limit 1");
    $sth->execute(array(':day' => $day));
    $data = $sth->fetchAll();
    $data['passage'] = $data[0]['passage'];
    $data['id'] = $data[0]['id'];
    $dbh = null;
  } catch ( PDOException $e) {
    print "Error!: " . $e->getMessage() . "<br/>";
    echo "$dbpath<br/>";
  }
  return $data;
}

function updateReading($dbpath,$id="") {
  try {
    $dbh = new PDO("sqlite:$dbpath/readings.db");
    $sth = $dbh->prepare("update readings set read=1 where id=:id");
    $sth->execute(array(':id' => $id));
    $dbh = null;
  } catch ( PDOException $e) {
    print "Error!: " . $e->getMessage() . "<br/>";
  }
}
?>
