<?php
include_once "funcs.php";
?>
<html>
<head>
<title>Slackers Reading for <?php echo $dayName; ?></title>
<meta name = "viewport" content = "width = device-width">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta name="apple-mobile-web-app-capable" content="yes">
<script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
</head>
<body>
<div id="content">
<?php
include "readings.php";
?>
</div>
</body>
</html>

