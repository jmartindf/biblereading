<?php
	$options = array(
		'key=IP',
		'include-first-verse-numbers=0',
		'include-verse-numbers=0',
		'include-headings=0',
		'include-audio-link=0',
		'passage='.urlencode($_GET['reading'])
	);
	$optionStr = join('&',$options);
	$url = "http://www.esvapi.org/v2/rest/passageQuery?";
	$query = $url.$optionStr;
	$response = "";
	
	$data = fopen($query, "r") ;

	if ($data) {
		while (!feof($data)) {
			$buffer = fgets($data, 4096);
			$response = $response.$buffer;
		}
		fclose($data);
	} else {
		die("fopen failed for url to webservice");
	}
	echo $response;
?>
