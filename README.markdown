This is a set of tools to make yearly Bible reading easier.

There are 3 main portions:

* The Bible plans themselves and tools to create, update, and read them as JSON files.
* An iOS web app, to track your progress through a yearly Bible reading plan.
* A set of scripts, using the ESV web API, to create eReader files containing the actual contents of each day’s reading.

Here is what’s in each directory.

### The Bible Plans

* **plans/input**—XML source, from Ben Edgington’s [M’Cheyne server](http://www.edginet.org/mcheyne/server.html), used to generate the M’Cheyne plan data. Also, the pipe-delimited text files, used to create other plans.
* **plans/tools**—Scripts used to turn the M’Cheyne raw data into a JSON plan file. Also, scripts used to take a pipe-delimited text file and turn it into a JSON plan file.
* **plans/output**—The actual reading plans themselves, after processing.

### The Web App

This is a small iPhone/iPod Touch web app to track progress through a yearly Bible reading plan. It allows you to check off each passage for a day and progress through the days of the year. At the moment, it only has support for the M'Cheyne reading plan.

All files are stored locally on the iPhone/iPod Touch and the settings are stored locally as well. The effect is akin to that of installing a native app through the App Store. It can be used offline. You don't need an account and it won't track your progress across devices.

* **experimental**—the start of some support for remotely loading a given day’s Bible reading. This may eventually be made a part of the web app, to allow you to read each passage directly from the app.
* **images**—The source images, used in the web app.
* **web**—The source for the web app.

### The eReader Files

I've also created a script that will download the daily passages from the ESV webservice and create a Kindle compatible mobi file, to read the daily readings directly.

* **mobi/output**—The actual eReader files, ready to be loaded onto a device.
* **mobi/tools**—Scripts for using the plan, and the API, to generate the eReader files.

