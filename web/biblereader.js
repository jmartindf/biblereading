var jQT = $.jQTouch({
    icon: 'checked_bible_retina.png',
    statusBar: 'black-translucent',
    preloadImages: [
        'themes/apple/img/chevron.png',
        'themes/apple/img/backButton.png',
        'themes/apple/img/toolbar.png'
        ],
	fixedViewport: true,
	startupScreen: "dwyl2-jesus-saves.png",
	fullScreen: true,
	initializeTouch: 'a, .touch'
});

var db;
var dropPlanTable = "drop table if exists plan;";
var dropReadingsTable = "drop table readings;";
var dropProgressTable = "drop table myprogress;";

var createPlanTable = "CREATE TABLE IF NOT EXISTS plan (" +
	"id INTEGER PRIMARY KEY AUTOINCREMENT," +
	"name varchar(255)" +
");";
var createReadingsTable = "CREATE TABLE IF NOT EXISTS readings (" +
	"id INTEGER PRIMARY KEY AUTOINCREMENT," +
	"plan INTEGER," +
	"day int," +
	"passage varchar(255)," +
	"dayindex int," + 
	"read boolean default 'n'" +
");";
var createProgressTable = "CREATE TABLE IF NOT EXISTS myprogress (" +
	"id INTEGER PRIMARY KEY AUTOINCREMENT," +
	"plan_id int," +
	"currentday int" +
");";
var insertPlan = "insert into plan(name) values (?)";
var insertReading = "insert into readings(plan, day, passage,dayindex) values (?, ?, ?, ?);";

var req;
var plan;
var currentDay;

$(document).ready(function(){
    $('#settings form').submit(saveSettings);
	$('#settings').bind('pageAnimationStart', loadSettings);
	$('#planload').click(loadData);
	$('#prevday').click(previousDay);
	$('#nextday').click(nextDay);
	loadSettings();
	
	var shortName = 'Bible Reader';
	var version = '1.0';
	var displayName = 'Bible Reader';
	var maxSize = 65536;
	db = openDatabase(shortName, version, displayName, maxSize);
	
	createTables();
	populateReadings();
});

function previousDay() {
	currentDay = parseInt(currentDay) - 1;
	if (parseInt(currentDay) < 0) currentDay = 0;
	if (parseInt(currentDay) > 364) currentDay = 364;
	localStorage.setItem(plan+"_currentDay", currentDay);
	loadSettings();
	populateReadings();
}

function nextDay() {
	currentDay = parseInt(currentDay) + 1;
	if (parseInt(currentDay) < 0) currentDay = 0;
	if (parseInt(currentDay) > 364) currentDay = 364;
	localStorage.setItem(plan+"_currentDay", currentDay);
	loadSettings();
	populateReadings();
}

function populateReadings() {
	$("#readingday").empty();
	$("#readinglist").empty();
	$("#realdate").empty();
	if ((!currentDay) || (!plan)) return;
	// get current plan -- use var plan
	// get current day -- use var currentDay
	// get readings for current plan day
	var myDate=new Date(new Date().getFullYear(),0,1);
	myDate.setDate(myDate.getDate()+parseInt(currentDay));
	var one_day=1000*60*60*24;
	var myDateS = myDate.toLocaleDateString();
	var daysBehind = Math.ceil((new Date().getTime()-myDate.getTime())/(one_day));
	$("#readingday").append("Day "+currentDay);
	$("#realdate").append(myDateS);
	if (daysBehind < 0) {
		adj = " days ahead";
		daysBehind = daysBehind * -1;
	} else {
		adj = " days behind";
	}
	$("#realdate").append("<br />" + daysBehind + adj); 
	db.transaction(
		function(transaction) {
			transaction.executeSql(
				'select * from readings where day=? and plan=? order by dayindex asc;',
				[currentDay, plan],
				function(transaction, results) {
					var numRows = results.rows.length;
					for (var i=0; i < numRows; i++) {
						var row = results.rows.item(i);
						var name = row['id'];
						var passage = row['passage'];
						var onchange;
						var checked;
						if (row['read']=='y') {
							checked = ' checked="checked"';
							onchange = "markRead(this.name,'n')";
						} else {
							checked = "";
							onchange = "markRead(this.name,'y')";
						}
						var output = '<li><input type="checkbox"'+checked+' name="'+name;
						output = output +'" id="'+'check-'+name+'" onchange="'+onchange;
						output = output + '" class="checkbox,touch" title="'+passage;
						output = output + '" value="'+name+'" />';
						output = output + '</li>';
						$("#readinglist").append(output);
						output = "";
					}
				},
				errorHandler
			);
		}
	);
}

function createTables() {
	/*
	db.transaction(
		function(transaction) {
			transaction.executeSql( dropPlanTable );
			transaction.executeSql( dropReadingsTable );
			transaction.executeSql( dropProgressTable );
		}
	);
	*/	
	db.transaction(
		function(transaction) {
			transaction.executeSql( createPlanTable );
			transaction.executeSql( createReadingsTable );
			transaction.executeSql( createProgressTable );
		}
	);	
}

function saveSettings() {
	currentDay = $('#day').val();
	plan = $('#plan').val();
	localStorage.planId = plan;
	localStorage.setItem(plan+"_currentDay", currentDay);
	db.transaction(
		function(transaction) {
			transaction.executeSql( 'update readings set read=? where day<?;', ['y',currentDay] );
			transaction.executeSql( 'update readings set read=? where day>=?;', ['n',currentDay] );
		}
	);	
	populateReadings();
    jQT.goBack();
    return false;
}

function loadSettings() {
	plan = localStorage.planId;
	currentDay = localStorage.getItem(plan + "_currentDay");
    $('#day').val(currentDay);
	$('#plan').val(plan);
}

function loadData() {
	req = new XMLHttpRequest();
	req.open("GET", "mcheyne.json", true); 
	req.onreadystatechange = addPlan;  // the handler 
	req.send(null);
	return false;
}

function nullDataHandler(transaction, results) { }

function errorHandler(transaction, error) {
    alert('Oops. Error was '+error.message+' (Code '+error.code+')');
    return true;
}

function markRead(id,state) {
	db.transaction(
		function(transaction) {
			transaction.executeSql(
				'update readings set read=? where id=?;',
				[state, id],
				function(transaction, results) {
					transaction.executeSql(
						"select count(id) as unread_count from readings where day=? and plan=? and read='n';",
						[currentDay, plan],
						function(transaction, results) {
							var numRows = results.rows.length;
							if (numRows == 1) {
								var count = results.rows.item(0)['unread_count'];
								if (count == 0) {
									currentDay = parseInt(currentDay) + 1;
									localStorage.setItem(plan+"_currentDay", currentDay);
									populateReadings();
								}
							}					
						},
						errorHandler
					);
				},
				errorHandler
			);
		}
	);
}

function addPlan() 
{ 
   if (req.readyState == 4) 
   { 
        var plan = eval('(' + req.responseText + ')');
		readings = plan.readings;
		numReadings = readings.length;
		db.transaction(
			function(transaction) {
				transaction.executeSql(
					insertPlan, [plan.name], 
					function (transaction, resultSet) {
						var planId = resultSet.insertId;
						localStorage.planId = planId;
						localStorage.setItem(planId + "_currentDay", 0);
						for ( var i=0; i<numReadings; ++i ) {
							reading = readings[i];
							day = reading['plan'];
							passage = reading['reference'];
							dayindex = reading['refcnt'];
							transaction.executeSql(insertReading, [planId, day, passage, dayindex], nullDataHandler, errorHandler);							
						}
						loadSettings();
						populateReadings();
						jQT.goBack();
					}, 
					errorHandler
				);
			}
		);
   }
} 
