#!/usr/bin/php
<?php
$data = file($argv[1], FILE_IGNORE_NEW_LINES|FILE_SKIP_EMPTY_LINES);
$readings = array();
$plan = array();
foreach ($data as $line) {
  $pieces = explode("|",$line);
  $day = intval($pieces[0]);
  $refcnt = intval($pieces[1]);
  $reference = $pieces[2];
  $readings[$day-1][$refcnt-1] = $reference;
}
$plan['name']="Blackhawk 2012";
$plan['readings']=$readings;
$json = json_encode($plan);
echo $json;
?>
