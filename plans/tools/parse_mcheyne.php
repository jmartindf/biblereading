<?php

class PlanParser {

	var $insidereading = false;
	var $insideday = false;
	var $tag = "";
	var $reference = "";
	var $description = "";
	var $link = "";
	var $day = "";
	var $refcnt = "";

	function startElement($parser, $tagName, $attrs) {
		if ($this->insidereading) {
			$this->tag = $tagName;
		} elseif ($tagName == "READING") {
			$this->insidereading = true;
		} elseif ($tagName == "DAY") {
			$this->insideday = true;
			$this->tag = $tagName;
			$this->day = "";
		}
	}

	function endElement($parser, $tagName) {
		if ($tagName == "READING") {
			$this->day = trim($this->day);
			$this->reference = trim($this->reference);
			$this->refcnt = trim($this->refcnt);
			printf("{'plan':'%d', 'reference':'%s', 'refcnt':'%d'},\n", $this->day, $this->reference, $this->refcnt);
			#printf("insert into reading(plan_id, day, passage, dayindex) values (1,%d,'%s',%d);\n",$this->day,$this->reference,$this->refcnt);
			#printf("%s @ %d\n",trim($this->reference),trim($this->day));
			//printf("<dt><b><a href='%s'>%s</a></b></dt>",
			//	trim($this->link),htmlspecialchars(trim($this->title)));
			//printf("<dd>%s</dd>",htmlspecialchars(trim($this->description)));
			$this->reference = "";
			$this->description = "";
			$this->link = "";
			$this->insidereading = false;
			$this->refcnt = $this->refcnt+1;
		}
		if ($tagName == "DAY") {
			$this->insideday = false;
		}
	}

	function characterData($parser, $data) {
		if ($this->insidereading) {
			switch ($this->tag) {
				case "REFERENCE":
				$this->reference .= $data;
				break;
				case "DESCRIPTION":
				$this->description .= $data;
				break;
				case "URL":
				$this->link .= $data;
				break;
			}
		} elseif ($this->insideday) {
			switch ($this->tag) {
				case "DAY":
				$this->day .= $data;
				break;
			}
		}
	}
}

	# parsing "carson_mcheyne_0.xml" obtained via "wget "http://www.edginet.org/mcheyne/server.php?bible=esv&cal=carson&day=0""
	// Create an XML parser
	$xml_parser = xml_parser_create();
	
	$plan_parser = new PlanParser();
	xml_set_object($xml_parser,$plan_parser);
	
	// Set the functions to handle opening and closing tags
	xml_set_element_handler($xml_parser, "startElement", "endElement");
	
	// Set the function to handle blocks of character data
	xml_set_character_data_handler($xml_parser, "characterData");
	
	// Open the XML file for reading
	$fname = $argv[1];
	$fp = fopen($fname,"r") or die("Error reading XML data.");
	
	// Read the XML file 4KB at a time
	while ($data = fread($fp, 4096))
		// Parse each 4KB chunk with the XML parser created above
		xml_parse($xml_parser, $data, feof($fp))
	or die(sprintf("XML error: %s at line %d\n", xml_error_string(xml_get_error_code($xml_parser)), xml_get_current_line_number($xml_parser)));

	// Close the XML file
	fclose($fp);
	
	// Free up memory used by the XML parser
	xml_parser_free($xml_parser);
?>
